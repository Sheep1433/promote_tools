import json

import redis
import time
import random

# r = redis.StrictRedis(host='proxy.cache-codis.staging.shopeemobile.com', port=9076, db=0)
g_rdb = redis.StrictRedis(host='proxy.cache-codis-sg2.i.test.sz.shopee.io', port=14014, db=0)

g_region = "id"
env = "test"

def get_val(key):
    a = g_rdb.get(key)
    return a


def set_val(key, val):
    g_rdb.set(key, val)
    return


# item详情
# val = "{\"item_id\":"+str(i)+",\"category_id\":70102,\"item_name\":\"ruishen test opv 20201030005 en\",\"items_sold\":11,\"visibility_scope\":0,\"status\":1,\"maintenance\":0,\"sold_out\":0,\"stock_available\":0,\"sold_out_shopeepay\":0,\"maintenance_shopeepay\":0,\"status_shopeepay\":1,\"visibility_scope_shopeepay\":0,\"item_name_local\":\"ruishen test opv 202010300045 id\",\"item_name_zhs\":\"\",\"valid_start_time\":1603990800,\"valid_end_time\":2147454840,\"opv_display_type\":0,\"opv_display_type_weight\":80,\"mall_name\":\"\",\"mall_name_local\":\"\",\"text_below_icon\":\"text en\",\"text_below_icon_local\":\"text id\",\"text_below_icon_zhs\":\"\",\"in_stock_shopeepay\":false,\"in_stock\":false,\"has_outlets\":true,\"has_outlets_both_online\":true,\"city_id\":{\"city_id\":[2,1,3]},\"flash_sale\":null,\"brand_id\":{\"brand_id\":[100002,100003,100532,100001]},\"user_tag\":{\"user_tag\":[\"default_all\"]},\"sku_name\":\"\",\"es_sku_name_local\":\"\",\"es_sku_name\":\"\",\"show_sequence\":0,\"mall_ids\":[0,1,2,3,4,5,6,7,8,9,10]}"

def build_detail_1(item_id):
    key = "{}_{}_es-sync-server_item-raw_{}".format(g_region, env, item_id)
    # r.delete(key)
    print("key1 : ", key)
    info = {
        "item_id": item_id,
        "category_id": 70102,
        "item_name": "ruishen test opv 20201030005 en",
        "items_sold": 11,
        "visibility_scope": 0,
        "status": 1,
        "maintenance": 0,
        "sold_out": 0,
        "stock_available": 0,
        "sold_out_shopeepay": 0,
        "maintenance_shopeepay": 0,
        "status_shopeepay": 1,
        "visibility_scope_shopeepay": 0,
        "item_name_local": "ruishen test opv 202010300045 id",
        "item_name_zhs": "",
        "valid_start_time": 1603990800,
        "valid_end_time": 2147454840,
        "opv_display_type": 0,
        "opv_display_type_weight": 80,
        "mall_name": "",
        "mall_name_local": "",
        "text_below_icon": "text en",
        "text_below_icon_local": "text id",
        "text_below_icon_zhs": "",
        "in_stock_shopeepay": False,
        "in_stock": True,
        "has_outlets": True,
        "has_outlets_both_online": True,
        "city_id": {
            "city_id": [2, 1, 3]
        },
        "flash_sale": None,
        "brand_id": {
            "brand_id": [107185,120167,120792,122755,122840]
        },
        "user_tag": {
            "user_tag": [
                "default_all"
            ]
        },
        "sku_name": "",
        "es_sku_name_local": "",
        "es_sku_name": "",
        "show_sequence": 0,
        "mall_ids": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    }
    # print(json.dumps(info))
    set_val(key, json.dumps(info))
    print(get_val(key))


def build_defail_2(itemId):
    module = "index-proxy"
    key = "{}_{}_{}_item_detail_info_{}".format(g_region, env, module, itemId)
    print("key2 : ", key)
    info = {
        "item_id": itemId,
        "tag_ids": ["160823", "160800", "160799", "160822"],
        "type_ids": ["1"],
        "cashback_type": "coin",
        "rate": 60,
        "flat": 0,
        "items_sold_7d": 0,
        "items_sold_30d": 0,
        "max_sold_30d": 110723,
        "max_sold_7d": 36523,
        "mis_tag_ids": [160802, 160822, 314576, 160805]
    }
    # print(json.dumps(info))
    set_val(key, json.dumps(info))

def build_detail_outlet(outlet_id):
    key = "{}_{}_index-proxy_forward-outlet-base_{}".format(g_region, env, outlet_id)
    # r.delete(key)
    print("key1 : ", key)
    info = {
    "outlet_id": outlet_id,
    "type_id": 1,
    "brand_id": 107185,
    "merchant_id": 102090,
    "tag_id_list": [
        0,
        102090,
        5,
        20,
        1,
        6,
        7
    ],
    "outlet_mis_tag": [
        160802,
        160814,
        160801
    ],
    "outlet_correct_brand": 118278
}
    # print(json.dumps(info))
    set_val(key, json.dumps(info))


def main():
    voucher_ids = [500151408,923456]
    for i in voucher_ids:
        build_detail_1(i)
        build_defail_2(i)
        build_detail_outlet(i)



if __name__ == '__main__':
    main()
    print(get_val("id_test_es-sync-server_item-raw_500151408"))
