# -*- coding: UTF-8 -*-
import csv
import os
import json
import time

# 日志文件路径
# file = '/Users/zhengqiang.zhang/PycharmProjects/pythonProject/脚本/pipeline_search_results_20220729_183259.txt'   # id地区
# file = '/Users/zhengqiang.zhang/PycharmProjects/pythonProject/脚本/pipeline_search_results_20220729_184928.txt'   # my地区
# file = '/Users/zhengqiang.zhang/PycharmProjects/pythonProject/脚本/pipeline_search_results_20220729_185338.txt'   # sg地区
# file = '/Users/zhengqiang.zhang/PycharmProjects/pythonProject/脚本/pipeline_search_results_20220729_190204.txt'   # vn地区
# file = '/Users/zhengqiang.zhang/PycharmProjects/pythonProject/脚本/pipeline_search_results_20220729_185919.txt'   # th地区
file = '/Users/zhengqiang.zhang/PycharmProjects/pythonProject/脚本/pipeline_search_results_20220729_185654.txt'   # ph地区



# file = '/Users/zhengqiang.zhang/Downloads/pipeline_search_results_20220719_100319.txt'
# file = "/Users/zhengqiang.zhang/PycharmProjects/pythonProject/脚本/results.txt"
# def
#
def word_extract(s: str):
    """传入一个字符串提取出其中的query，scene，city_id, public_id，返回列表"""
    s = s.replace("\\\\\\", "")
    # 匹配query
    pattern = re.compile(r'"query":.*?,"ab_test"')
    res = []
    result = pattern.findall(s)
    if len(result) == 1:
        result = result[0]
        result = result.replace('"query":', "")
        result = result.replace(',"ab_test"', "")
        query = eval(result)
        query = query.replace(" ", "%20")
        res.append(query)

    # 匹配city_id
    publish_id = "publish_id"
    if publish_id in s:
        pattern = re.compile(r'"city_id":.*?,')
        result = pattern.findall(s)
        if len(result) == 1:
            result = result[0]
            result = result.replace('"city_id":', "")
            result = result.replace(',', "")
            city_id = eval(result)
            res.append(city_id)
    else:
        pattern = re.compile(r'"city_id":.*?}')
        result = pattern.findall(s)
        if len(result) == 1:
            result = result[0]
            result = result.replace('"city_id":', "")
            result = result.replace('}', "")
            city_id = eval(result)
            res.append(city_id)

    # res = str(res).replace("'", "\"")

    if len(res) == 2:
        return res
    return []


if __name__ == '__main__':
    import re

    # with open("mytest.csv", "w") as c:
    #     with open(file, 'r') as f:
    #         s = f.read()
    #         pattern1 = re.compile(r'req={.*?}')  # 查找数字s
    #         result1 = pattern1.findall(s)
    #         res = []
    #         for s in result1:
    #             ls = word_extract(s)
    #             res.append(ls)
    #             writer = csv.writer(c, delimiter='|')
    #             if ls:
    #                 writer.writerow(ls)

    with open(file, 'r') as f:
        s = f.read()
        pattern1 = re.compile(r'req={.*?}')  # 查找数字s
        result1 = pattern1.findall(s)
        res = []
        for s in result1:
            ls = word_extract(s)
            if ls:
                res.append(ls)

    with open("../大促压测数据准备/88/file_text/ph_res.txt", "w") as c:
        # res = [["Msd", 1, 2], ["M", 0,1]]
        for ls in res:
            s = "\"" + ls[0] + "\"" + "|" + str(ls[1])
            s = ls[0] + "|" + str(ls[1])
            c.write(s)
            c.write("\n")
