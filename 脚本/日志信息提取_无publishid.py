# -*- coding: UTF-8 -*-
import csv
import os
import json
import time

# 日志文件路径
file = '/Users/zhengqiang.zhang/Downloads/pipeline_search_results_20220720_161637.txt'


# file = '/Users/zhengqiang.zhang/Downloads/pipeline_search_results_20220719_100319.txt'
# file = "/Users/zhengqiang.zhang/PycharmProjects/pythonProject/脚本/results.txt"
# def
#
def word_extract(s: str):
    """传入一个字符串提取出其中的query，scene，city_id, public_id，返回列表"""
    s = s.replace("\\\\\\", "")
    # 匹配query
    pattern = re.compile(r'"query":.*?,"ab_test"')
    res = []
    result = pattern.findall(s)
    if len(result) == 1:
        result = result[0]
        result = result.replace('"query":', "")
        result = result.replace(',"ab_test"', "")
        query = eval(result)
        res.append(query)
    # 匹配scene
    pattern = re.compile(r'"scene":.*?,"city_id"')
    result = pattern.findall(s)
    if len(result) == 1:
        result = result[0]
        result = result.replace('"scene":', "")
        result = result.replace(',"city_id"', "")
        # result = result.replace("\"","'")
        scene = eval(result)
        res.append(scene)
    # 匹配city_id
    pattern = re.compile(r'"city_id":.*?}')
    result = pattern.findall(s)
    if len(result) == 1:
        result = result[0]
        result = result.replace('"city_id":', "")
        result = result.replace('}', "")
        city_id = eval(result)
        res.append(city_id)
    # res = str(res).replace("'", "\"")
    return res if len(res) == 3 else []



if __name__ == '__main__':
    import re

    with open(file, 'r') as f:
        s = f.read()
        pattern1 = re.compile(r'req={.*?}')  # 查找数字s
        result1 = pattern1.findall(s)
        res = []
        for s in result1:
            ls = word_extract(s)
            if ls:
                res.append(ls)

    with open("../大促压测数据准备/88/file_text/mytest.csv", "w") as c:
        # res = [["Msd", 1, 2], ["M", 0,1]]
        for ls in res:
            s = "\"" + ls[0] + "\"" + "|" + str(ls[1]) + "|" + str(ls[2])
            c.write(s)
            c.write("\n")
