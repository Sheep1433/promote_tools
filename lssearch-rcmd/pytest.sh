#virtualenv venv
#source ./venv/bin/activate
#pip3 install -r requirements.txt

current_data=$(date "+%Y%m%d%H%M%S")
pytest -s ./testcases --alluredir target/allure-results 2>&1 | tee run_test.log
allure generate target/allure-results -o ./reports/ --clean
#allure open ./reports/
result_code=$?
echo "result_code=$result_code" > res.tmp