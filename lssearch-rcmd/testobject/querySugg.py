import sys

from lib.base_grpc import BaseRpc


class QuerySugg(BaseRpc):
    def __init__(self, url, server_name):
        super(QuerySugg, self).__init__(url, server_name)

        self.req_dict = {
            "user_id": 24,
            "query": "Te",
            "scene": 0,
            "ab_test": "dsa=xxx,ls.suggestion_dict=test2",
            "city_id": 51,
            "publish_id": "15e412f8-a462-43a2-8f12-99d93c919a0f"
        }

if __name__ == '__main__':
    test = QuerySugg('10.103.155.23:8080', 'o2oalgo_querysugg')
    resp = test.send_rpc_request("GetLsQuerySuggestList", test.req_dict)
    print(resp.query_suggest_list)

