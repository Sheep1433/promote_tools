import allure
import pytest

from testobject.querySugg import QuerySugg
from utils.read_conf import ReadConf
from utils.read_yaml import read_yaml
# from utils.zk_operation import ZkOperation


@pytest.fixture(scope='class', autouse=True)
@allure.step("连接接口服务")
def Init_algoquerySugg():
    conf = ReadConf()
    # 通过zk获取url，因为不知道服务名和cid规则，暂时不使用
    # url = ZkOperation()._get_server("foodalgo-score", conf['env'], conf['cid'])
    url = '10.103.187.27:8080'
    global querySugg
    querySugg = QuerySugg(url, 'o2oalgo_querysugg')
    print("create rpc server sucess.....")


@allure.story("结果匹配功能")
class TestQuerySugg():
    @pytest.mark.p0
    @allure.title("单字母简单匹配")
    @pytest.mark.parametrize("query, city_id", read_yaml("lssearchquerysugg.yaml"))
    def test_querysugg_simple(self, query, city_id):
        query_data = {
            "query": query,
            "city_id": city_id
        }
        querySugg.req_dict.update(query_data)
        resp = querySugg.send_rpc_request("GetLsQuerySuggestList", querySugg.req_dict)
        resp_data = resp.query_suggest_list
        if len(resp_data) != 0:
            assert len(resp_data) > 0
            for sugg in resp_data:
                # 断言分数大于0
                assert sugg.score > 0
                # 断言结果包含词
                # 断言数据长度不为0
                assert query_data["query"].upper() in sugg.query_suggest.upper()

    @pytest.mark.p0
    @allure.title("店名完整匹配")
    @pytest.mark.parametrize("query, city_id", read_yaml("lssearchquerysugg02.yaml"))
    def test_querysugg_total(self, query, city_id):
        query_data = {
            "query": query,
            "city_id": city_id
        }
        querySugg.req_dict.update(query_data)
        resp = querySugg.send_rpc_request("GetLsQuerySuggestList", querySugg.req_dict)
        resp_data = resp.query_suggest_list
        assert len(resp_data) > 0
        for sugg in resp_data:
            assert sugg.score > 0
            assert query_data["query"].upper() in sugg.query_suggest.upper()
