res_code=0
res_flag="☑︎success"  # ☒failed
log_file="run_test.log"
#注释


get_err_case(){
    #执行异常
    echo here is get_err_case
    if [ `grep -c "short test summary info" $log_file` -eq 0 ];then
        echo "unexpect result" && return
    fi
    #collected 72 items / 2 errors / 70 selected
    #提取报错的用例
    err_num=$(sed -n '/short test summary info/,$ p' $log_file|grep -e '^ERROR'|wc -l)
    i=0
    if [ $err_num -ne 0 ];then
        printf "%s\\\\n" "================================异常 ${err_num}======================================"
        sed -n '/short test summary info/,$ p' $log_file|grep -e '^ERROR'|while read line
        do
            if [ $i -ge 6 ];then
                printf "%s\\\\n" "...$((err_num-i)) more"
                break
            fi
            printf "%s\\\\n" "$line"
            i=$((++i))
        done
    fi
    #执行失败的用例
    fail_num=$(sed -n '/short test summary info/,$ p' $log_file|grep -e '^FAILED'|wc -l)
    if [ $fail_num -ne 0 ];then
        printf "%s\\\\n" "================================失败 ${fail_num}======================================"
        #提取失败用例名称
        case_name=(`sed -n '/FAILURES/,/short test summary info/p' $log_file|sed -n "/@allure.title/p"|sed 's|.*\("[^"]*"\).*|\1|g'|xargs`)
        [ $i -ge 4 ] && num=$((10-$i)) || num=$((8-$i))
        i=0
        sed -n '/short test summary info/,$ p' $log_file|grep -e '^FAILED'|while read line
        do
            printf "%s\\\\n" "----------------------------------------------------------------------------------------"
            if [ $i -ge $num ];then
                printf "%s\\\\n" "...$((fail_num-num)) more: $BUILD_URL"
                break
            fi
            printf "%s\\\\n" "$((i+1)):${case_name[$i]}"
            printf "%s\\\\n" "$line"
            i=$((++i))
        done
    fi
}

get_case_rate(){
  echo here is get_case_rate
    ##collected 72 items ##collected 72 items / 2 errors / 70 selected
    case_all=$(grep -e "collected[0-9 ]\+item" $log_file|head -1|sed -r 's|collected[ ]*([0-9]+).*|\1|')
    #提取用例数据
    info=`grep "======" $log_file|tail -1`
    [ `echo "$info"|grep -c "passed"` -eq 0 ] && case_pass=0 || case_pass=$(echo "$info"| sed -r 's|.*[^0-9]+([0-9]+)[ ]*passed.*|\1|')
    [ `echo "$info"|grep -c "failed"` -eq 0 ] && case_fail=0 || case_fail=$(echo "$info"| sed -r 's|.*[^0-9]+([0-9]{1,})[ ]*failed.*|\1|')
    [ `echo "$info"|grep -c "error"` -eq 0 ] && case_error=0 || case_error=$(echo "$info"| sed -n 's|.*[^0-9]\{1,\}\([0-9]\{1,\}\)[ ]*error.*|\1|p')
    if [ "$case_pass" -eq 0 ];then
        case_rate="0"
    elif [ "$case_all" -gt 0 ];then
        case_rate=$(echo "scale=2; ${case_pass}*100/${case_all}"|bc)
    else
        #case_rate=$((case_pass/$((case_pass+case_fail))))
        #case_rate=$(awk -v a=$case_pass,b=$case_fail '{printf "%.2f",a/(a+b)}')
        case_rate=$(echo "scale=2; ${case_pass}*100/($case_pass+$case_fail+$case_error)"|bc)
    fi
    echo "成功率=$case_rate% | succ=[$case_pass] - fail=[$case_fail] - error[$case_error]"
}

#curl -i -X POST -H 'Authorization: Basic WlpROjExMWIyMWE1MzMxMzI2MGI1OGMyYmRmOGVjNjk3NDExOTU=' http://127.0.0.1:8080/job/12/build

WORKSPACE=./
cd ${WORKSPACE}
res_code=$(cat res.tmp|awk -F'=' '{print $2}')
echo $WORKSPACE
if [ "$res_code" -ne 0 ];then
  res_flag="error"
  if [ -r $log_file ];then
    err_msg=`cat $log_file|grep -e "error"`
    echo --------------------------------
    echo $err_msg
  else
    err_msg="执行异常，请登录查看"
  fi
else
  res_flag="`get_case_rate`"
  case_list=`printf "%s" "$(get_err_case)"`
fi
echo "good"
git push
#echo "msg = $case_list"
BUILD_TAG=tmp_BUILD_TAG
BUILD_URL="http://127.0.0.1:8080/job/12/"
res_msg="任务: $BUILD_TAG\n运行结果: ${res_flag}\n报告查看: ${BUILD_URL}allure/\n失败用例:\n${case_list}"
err_msg="任务: $BUILD_TAG\n运行结果: ${res_flag}\n异常信息: ${err_msg}\n构建地址：${BUILD_URL}"
echo ------------------


#data='{"tag":"text","text":{"content":"'$([ "$res_code" -ne 0 ] && echo "${err_msg}" || echo "${res_msg}" )'\n","mentioned_list": [],"at_all":false}}'
content=$([ "$res_code" -ne 0 ] && echo "${err_msg}"|tr -d "\\n" || echo "${res_msg}"|tr -d "\\n")
#data='{"tag":"text","text":{"content":"'$([ "$res_code" -ne 0 ] && echo "${err_msg}"|tr -d "\\n" || echo "${res_msg}"|tr -d "\\n")'\n","mentioned_list": [],"at_all":false}}'
data='{"tag":"text","text":{"content":"'${content}'","mentioned_list": [],"at_all":false}}'
#echo $data

curl -i -X POST -H 'Content-Type: application/json' -d "$data" https://openapi.seatalk.io/webhook/group/P6YxRdmqQNyjuJJSxGVU1Q

exit 0
