# coding: utf-8
# @File     :zk_operation.py
# @Time     :2021/4/6
# @Author   :ruru.xie
# @Software :PyCharm
# @Desc     :调用Zk获取service对应Pod IP


from kazoo.client import KazooClient
from utils.read_conf import ReadConf


class ZkOperation(object):
    def __init__(self):
        self.conf = ReadConf()
        self._zk = KazooClient(hosts=self.conf["zk"]["host"])
        self._zk.start()

    # 输入service_name注意中间符号为"-"
    def _get_server(self, services_name, env, cid):
        children = self._zk.get_children("/services/" + services_name + "-" + env + "-" + cid + "/")
        print(children)
        return "".join(children[0])

    def _stop(self):
        self._zk.stop()


if __name__ == "__main__":
    url = ZkOperation()._get_server("foodalgo-shippingfee", "test", "xx")
    print(url)
