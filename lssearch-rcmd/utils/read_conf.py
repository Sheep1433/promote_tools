#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import os
import yaml


class ReadConf:
    """获取元素"""

    def __init__(self, name="conf.yaml"):
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        ELEMENT_PATH = os.path.join(BASE_DIR, 'conf')
        self.file_name = name
        self.element_path = os.path.join(ELEMENT_PATH, self.file_name)
        if not os.path.exists(self.element_path):
            raise FileNotFoundError("%s 文件不存在！" % self.element_path)
        with open(self.element_path, encoding='utf-8') as f:
            self.data = yaml.safe_load(f)

    def __getitem__(self, item):
        """获取属性"""
        data = self.data.get(item)
        # print(data)
        return data


if __name__ == '__main__':
    test = ReadConf()
    # print(test['db'][env'database'])

    print(str.upper(test['env']))

