# coding=utf-8
import grpc

import importlib
import sys
import os
import re
import allure

# curPath = os.path.abspath((os.path.dirname(__file__)))
# rootPath = os.path.split(curPath)[0]
# pbPath = os.path.join(rootPath, 'pb2')
# sys.path.append(rootPath)
# sys.path.append(pbPath)
# sys.path.append(curPath)


class BaseRpc(object):
    def __init__(self, url, proto, proto_suffix=''):
        channel = grpc.insecure_channel(url)
        # 导入pb2,rpc_pb2协议文件
        self.pb2_grpc = importlib.import_module('pb2.' + proto + proto_suffix + '.' + proto + '_pb2_grpc')
        self.pb2 = importlib.import_module('pb2.' + proto + proto_suffix + '.' + proto + '_pb2')



        self.pb2_grpc_path = self.pb2_grpc.__file__
        self.pb2_grpc_data = self.__get_pb2_grpc__()

        self.server = self.__get_server__()
        print("导入模块")
        print(self.pb2_grpc)
        print(self.pb2)
        self.stub = getattr(self.pb2_grpc, self.server)(channel)

    def __get_pb2_grpc__(self):
        with open(self.pb2_grpc_path, 'r') as f:
            data = f.read()
        return data

    def __get_server__(self):

        '''正则提取stub服务的名称'''

        pattern = 'class (.+?Stub)\\(object\\):'
        try:
            cl = re.findall(pattern, self.pb2_grpc_data)
            if len(cl) != 1:
                raise "stub服务名获取失败"
        except Exception as e:
            print('生成pb2_grpc文件中的stub数量错误', repr(e))
            raise
        return cl[0]

    def __get_request_set__(self, rpc_methord):
        "正则提取rpc接口中的request_serializer的值"
        pattern = self.server.replace("Stub",
                                      "") + '/' + rpc_methord + '\',\n[\\s\\S]*?request_serializer=([\\s\\S]*?),\n'
        try:
            rq = re.findall(pattern, self.pb2_grpc_data)
            print(rq)
            if len(rq) != 1:
                raise "request_serializer提取失败"
        except Exception as e:
            print("生成pb2_grpc文件中的req数量错误：", repr(e))
        rq = rq[0].split('.')[:-1]
        print("调用的方法")
        print(rq)
        return rq

    def __convreq__(self, data):
        print(data)
        req_str = ''
        if len(data):
            for i in list(data.keys()):
                if isinstance(data[i], str):
                    req_str += str(i) + '=\'' + str(data[i]) + '\','
                else:
                    req_str += str(i) + '=' + str(data[i]) + ','
            return req_str[:-1]  # 转换成key=value,的字符串格式参数
        else:
            return 0

    def send_rpc_request(self, rpc_methord, data):
        req_methord = self.__get_request_set__(rpc_methord)

        call = "getattr(self.pb2,\'" + req_methord[1] + "\')"

        func = getattr(self.stub, rpc_methord)

        try:
            response = func(eval(call)(**data))
        except Exception as e:
            # print("接口返回异常,code is:%s, details:%s" % (e.code(), e.details()))
            print("接口返回异常 : %s" % str(e))
            response = str(e)

        # print("接口返回")
        # print(response)
        with allure.step("接口的返回内容: {0}".format(response)):
            pass

        return response


if __name__ == "__main__":
    test = BaseRpc('10.103.155.23:8080', 'o2oalgo_querysugg')
    test_data = {
        "user_id": 24,
        "query": "Te",
        "scene": 0,
        "ab_test": "dsa=xxx,ls.suggestion_dict=test2",
        "city_id": 51,
        "publish_id": "15e412f8-a462-43a2-8f12-99d93c919a0f"
    }

    # print(type(test_data))

    resp = test.send_rpc_request("GetLsQuerySuggestList", test_data)
    print(resp.query_suggest_list)
