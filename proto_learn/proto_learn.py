import os.path
import re
import subprocess

s =os.path.dirname(os.path.abspath(__file__))
base_path = os.path.join(s, "proto")
ls = os.listdir(base_path)

def getprotofilelist(ls):
    res = []
    for i in ls:
        path = os.path.join(base_path, i)
        if os.path.isdir(path):
            for file in os.listdir(path):
                absolut_file_path = os.path.join(path, file)
                if absolut_file_path.endswith(".proto"):
                    res.append(absolut_file_path)
    return res

# def replace(oldfile, newfile):
#     with open(oldfile, 'r', encoding='utf-8') as fr:
#         with open(newfile, 'w', encoding='utf-8') as fw:
#             i = 0
#             for line in fr:
#                 i += 1
#                 # print(line)
#                 match_import = re.search(r'import "common/.*?";', line, re.M | re.I)
#                 if match_import:
#                     command = f"python -m grpc_tools.protoc --python_out=. --grpc_python_out=. -I. o2oalgo_gateway.proto
#                     subprocess.call("")
#                 if i >10:
#                     break

def build(file):
    with open(file, 'r', encoding='utf-8') as f:
        for line in f:
            match_import = re.search(r'import "common/.*?";', line, re.M | re.I)
            if match_import:
                command = f"python3 -m grpc_tools.protoc --python_out=. --grpc_python_out=. -I. -I../ lssearch_server.proto"
                subprocess.call(command)
                return
        command = f"python3 -m grpc_tools.protoc --python_out=. --grpc_python_out=. -I. {file}"
        subprocess.call(command)

if __name__ == '__main__':
    res = getprotofilelist(ls)
    # print(res)
    # print(res)
    build(res[1])

