with open("run_test.log", 'r', encoding='utf-8') as f:
    a = f.read()

import re

print(a)
failed_pattern = r'(\d+) failed,'
passed_pattern = r' (\d+) passed '
total_pattern = r'collected (\d+) items'
match_failed = re.search(failed_pattern, a, re.M | re.I)
match_passed = re.search(failed_pattern, a, re.M | re.I)
match_total = re.search(total_pattern, a, re.M | re.I)
print(match_failed.group(1))
print(match_passed.group(1))
print(match_total.group(1))
pattern = r'FAILED testcases.*?\n'
ls = re.findall(pattern, a)
print(ls)
title_pattern = r'@allure.title[(]"(.*?)"[)]'
match_title = re.findall(title_pattern, a)
print(match_title)