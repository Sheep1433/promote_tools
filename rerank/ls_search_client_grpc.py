#!/usr/bin/env python
# encoding: utf-8
"""
@author: peigen.jin
@file: ls_search_client_grpc.py
@time: 2022/7/26 2:41 PM
"""
import json
import os.path
import time
import grpc
import sys
import uuid
import ls_search_pb2, ls_search_pb2_grpc




class LsSearchGrpcClient:
    def __init__(self, ip_port, ab_test_map=None, sleep_time=0.05, print_step=1, max_try=3):
        # 测试环境端口是9087，live环境是9088
        self.ip_port = ip_port
        self.sleep_time = sleep_time
        self.print_step = print_step
        self.max_try = max_try
        default_ab_test = {"ls.auto_correction": "withcorrection",
                           "ls.search_ranking": "Experiment",
                           "ls.co_search_brand": 'brand_name_b1',
                           "ls.search_relevance_tab": "Test2"}
        if ab_test_map is not None:
            assert isinstance(ab_test_map, dict)
            for k, v in ab_test_map.items():
                default_ab_test[k] = v
        self.ab_test = default_ab_test

        self.conn = grpc.insecure_channel(ip_port)
        self.client = ls_search_pb2_grpc.LSSearchServerManagerStub(channel=self.conn)
        self.process_num = 0

    def search(self, input_paras):
        assert isinstance(input_paras, dict)
        if self.process_num % self.print_step == 0:
            print('process {}, input_paras:{} {}'.format(self.process_num, input_paras, time.ctime()))
        self.process_num += 1

        if 'outlet_ids' in input_paras:
            req = ls_search_pb2.OutletSearchReq(
                publish_id=input_paras.get('publish_id', "{}_{}".format(input_paras['query_word'], uuid.uuid4().hex)),
                search_id=input_paras.get('search_id', "{}_{}".format(input_paras['query_word'], uuid.uuid4().hex)),
                session_id=input_paras.get('session_id', "{}_{}".format(input_paras['query_word'], uuid.uuid4().hex)),
                user_id=input_paras.get('user_id', ''),
                city_id=input_paras['city_id'],
                latitude=input_paras['latitude'],
                longitude=input_paras['longitude'],
                page_num=input_paras.get('page_num', 1),
                page_size=input_paras.get('page_size', 15),
                rank_type=input_paras.get('rank_type', 0),
                query_word=input_paras['query_word'],
                ab_test_map=input_paras.get('ab_test_map', self.ab_test),
                outlet_ids=input_paras['outlet_ids'],
                user_tags=['default_all']
            )
        else:
            req = ls_search_pb2.OutletSearchReq(
                publish_id=input_paras.get('publish_id', "{}_{}".format(input_paras['query_word'], uuid.uuid4().hex)),
                search_id=input_paras.get('search_id', "{}_{}".format(input_paras['query_word'], uuid.uuid4().hex)),
                session_id=input_paras.get('session_id', "{}_{}".format(input_paras['query_word'], uuid.uuid4().hex)),
                user_id=input_paras.get('user_id', ''),
                city_id=input_paras['city_id'],
                latitude=input_paras['latitude'],
                longitude=input_paras['longitude'],
                page_num=input_paras.get('page_num', 1),
                page_size=input_paras.get('page_size', 15),
                rank_type=input_paras.get('rank_type', 0),
                query_word=input_paras['query_word'],
                ab_test_map=input_paras.get('ab_test_map', self.ab_test),
                user_tags=['default_all']
            )
        try_num = 0
        repeat_num = 0
        while try_num < self.max_try:
            try:
                time.sleep(self.sleep_time)
                res = self.client.LSOutletSearch(req)
                outlet_infos = [
                    {'outlet_id': t.outlet_id, 'outlet_name': t.outlet_name,
                     'distance': t.distance, 'items_sold': -1,
                     'score': t.score} for t in res.outlet_infos
                ]
                score_list = [t['score'] for t in outlet_infos]
                if sum(score_list) < 0.000001 and max(score_list) < 0.00001 and min(score_list) > -0.00001:
                    print('error: 分数全为0', input_paras['query_word'], '无限循环重复请求:', repeat_num)
                    # 超时无限循环
                    time.sleep(0.1)
                    repeat_num += 1
                    # try_num += 1
                elif len(outlet_infos) > 0:
                    return outlet_infos
                else:
                    try_num += 1
            except Exception as error:
                print('**** new ls search error **** ', try_num)
                print('error info:', error)
                try_num += 1
        return []

    def search_debug(self, input_paras):
        assert isinstance(input_paras, dict)
        if self.process_num % self.print_step == 0:
            print('process {}, input_paras:{} {}'.format(self.process_num, input_paras, time.ctime()))
        self.process_num += 1

        req = ls_search_pb2.OutletSearchReq(
            publish_id=input_paras.get('publish_id', "{}_{}".format(input_paras['query_word'], uuid.uuid4().hex)),
            search_id=input_paras.get('search_id', "{}_{}".format(input_paras['query_word'], uuid.uuid4().hex)),
            session_id=input_paras.get('session_id', "{}_{}".format(input_paras['query_word'], uuid.uuid4().hex)),
            user_id=input_paras.get('user_id', ''),
            city_id=input_paras['city_id'],
            latitude=input_paras['latitude'],
            longitude=input_paras['longitude'],
            page_num=input_paras.get('page_num', 1),
            page_size=input_paras.get('page_size', 15),
            rank_type=input_paras.get('rank_type', 0),
            query_word=input_paras['query_word'],
            ab_test_map=input_paras.get('ab_test_map', self.ab_test),
            user_tags=['default_all'],
            # request_param=param,
        )
        print("-================-=")
        print(req)

        try_num = 0
        repeat_num = 0
        while try_num < self.max_try:
            try:
                time.sleep(self.sleep_time)
                res = self.client.LSOutletSearchDebug(req)
                # print('respone:', res)
                debug_info = res.debug_stream
                debug_info = json.loads(debug_info)
                return debug_info
            except Exception as error:
                print('**** new ls search error **** ', try_num)
                print('error info:', error)
                try_num += 1
        return None



if __name__ == "__main__":
    # ip获取路径 https://kubernetes.devops.i.sz.shopee.io/applications/tenants/1041/projects/lssearchandrcmd/applications/lssearchandrcmd-lssearchsearchsvr/deploys/lssearchandrcmd-lssearchsearchsvr-live-id/clusters/LIVE-ID:sg5-k8s-live-001?azType=kubernetes&az=sg5&componentType=pipeline-deployment-enhance2
    test_ip = '10.103.140.181:9087'
    # test_ip = '10.101.82.124:9088'
    # test_ip = '10.103.140.181:8080'
    # test_ip ="10.129.109.11:9087"
    # live_ip = "10.101.75.168:9088"
    # live_ip = '10.174.192.75:9088' #sg
    ab_test = {"ls.auto_correction": "withcorrection", "ls.search_ranking": "Experiment",
               "ls.co_search_brand": "brand_name_b1", "ls.search_relevance_tab": "Test2",
               "ls.search_relevance_ranking": "Test1", "ls.search_relevance_model": "Test_1",
               "ls.search_order_ranking": "test1"
    }
    client = LsSearchGrpcClient(ip_port=test_ip, ab_test_map=ab_test)
    print(time.ctime())
    input_paras = {
        "query_word": 'kfc',
        "search_id": "test12345511312",
        "city_id": 1,
        "latitude": -7.690949,
        "longitude": 112.9033528,
        "page_size": 5,
    }
    print('test:', input_paras)
    input_paras['search_id'] = 'debug123'
    # res = client.search(input_paras=input_paras)
    # print('search接口:', res)
    input_paras['search_id'] = 'sg_debug123'
    res = client.search_debug(input_paras=input_paras)
    print('debug res:', res)
    print('***search debug接口:****')
    for k, v in res.items():
        print("*************", k, len(v))
        print("{} --> {}".format(k, v))
    print(time.ctime())
    print()