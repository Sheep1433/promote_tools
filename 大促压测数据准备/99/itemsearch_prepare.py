import os
import re

def prase_text2(path,dest):
    # user_id,city_id,in_tocks,long, lat,query,categ,
    with open(path, 'r', encoding='utf-8') as fr:
        with open(dest, 'w', encoding='utf-8') as fw:
            total = 0
            for line in fr:
                line = line.replace("\\\\\\", "")
                print(line)
                rline = {}
                match_user_id = re.search(r'user_id:(\d+)', line, re.M | re.I)
                match_city_id = re.search(r'city_id:(\d+)', line, re.M | re.I)
                match_in_stock_only = re.search(r'in_stock_only:(.*?) ', line, re.M | re.I)
                match_longitude = re.search(r'longitude:(\-?\d+\.?\d+)', line, re.M | re.I)

                match_latitude = re.search(r'latitude:(\-?\d+\.?\d+)', line, re.M | re.I)
                match_query = re.search(r'keyword:\\"(.*?)\\"', line, re.M | re.I)
                match_category_id = re.search(r'category_id:(\d+)', line, re.M | re.I)
                print(f"{match_city_id}- {match_in_stock_only} -{match_longitude}- {match_query} - {match_latitude}-{match_category_id}")
                if  match_city_id and match_in_stock_only and match_longitude and match_query and match_latitude and match_user_id:
                    rline['city_id'] = match_city_id.group(1)
                    rline['in_stock_only'] = match_in_stock_only.group(1)
                    rline['user_id'] = match_user_id.group(1)
                    rline['longitude'] = match_longitude.group(1)
                    rline['latitude'] = match_latitude.group(1)
                    rline['query'] = match_query.group(1)
                    rline['category_id'] = match_category_id.group(1)

                    # print(rline)
                    s = rline['city_id'] + '|"' +rline['in_stock_only']  +'"|'+ rline['longitude'] + '|' + rline['latitude'] + '|"' + rline['query'] + '"|' + rline['category_id'] + '|' + rline['user_id']
                    fw.write(s + '\n')


# os.mkdir("234")
file_path = "/Users/zhengqiang.zhang/PycharmProjects/promote_tools/大促压测数据准备/99/file/压测原始数据/itemsearch"
root_path = os.path.dirname(os.path.dirname(file_path))
for ls in os.listdir(file_path):
    if not ls.startswith("."):
        new_file_path = os.path.join(file_path, ls)
        for file in os.listdir(new_file_path):
            if file.endswith(".txt"):
                absolut_file_path = os.path.join(new_file_path, file)
                filepath_list = os.path.dirname(absolut_file_path).split(os.sep)
                area = filepath_list[-1]
                method = filepath_list[-2]
                new_path = os.path.join(root_path, method)
                if not os.path.isdir(new_path):
                    os.mkdir(new_path)
                final_file = f"{new_path}{os.sep}{method}_{area}.csv"
                prase_text2(absolut_file_path, final_file)




