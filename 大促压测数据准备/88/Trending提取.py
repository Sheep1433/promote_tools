# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import re
import json

def prase_text(path,dest):
    with open(path, 'r', encoding='utf-8') as fr:
        with open(dest, 'w', encoding='utf-8') as fw:
            for line in fr:
                rline = {}
                match_city_id = re.search(r'city_id:\d+', line, re.M | re.I)
                if match_city_id:
                    rline['city_id'] = match_city_id.group().split(':')[1]
                match_history = re.findall(r'history_queries:.*?}\s', line)
                if rline:
                    rline['history_queries'] = []
                for element in match_history:
                    match_history_query = re.search(r'history_query:.*?\\"\s', element, re.M | re.I)
                    match_timestap = re.search(r'timestamp:.*?}', element, re.M | re.I)
                    if match_history_query and match_timestap:
                        rline['history_queries'].append({"keyword":match_history_query.group().split(":")[1].replace("\\\"","").strip(),"timestamp": match_timestap.group().split(":")[1][0:-1] })
                if rline:
                    print(rline)
                    fw.write(json.dumps(rline)+'\n')

def prase_text2(path,dest):
    '''
    更简单的版本，不用split
    :param path:
    :param dest:
    :return:
    '''
    with open(path, 'r', encoding='utf-8') as fr:
        with open(dest, 'w', encoding='utf-8') as fw:
            total = 0
            for line in fr:
                if "TrendingSearch" in line:
                    rline = {}
                    match_city_id = re.search(r'city_id:(\d+)', line, re.M | re.I)
                    match_longitude = re.search(r'longitude:(\-?\d+\.?\d+)', line, re.M | re.I)
                    match_latitude = re.search(r'latitude:(\-?\d+\.?\d+)', line, re.M | re.I)
                    match_page = re.search(r'page_num:(\d+)', line, re.M | re.I)
                    if match_city_id and match_page and match_latitude and match_longitude:
                        rline['city_id'] = int(match_city_id.group(1))
                        rline['longitude'] = float(match_longitude.group(1))
                        rline['latitude'] = float(match_latitude.group(1))
                        rline['page'] = int(match_page.group(1))
                    if rline:
                        rline['history_queries'] = []
                        history_querys = re.findall(r'history_query:\\"(.*?)\\"\stimestamp:(\d+)', line)
                        total += 1
                        for element in history_querys:
                            rline['history_queries'].append({'keyword':element[0],"timestamp":int(element[1])})

                            # print(rline)

                            fw.write(json.dumps(rline)+'\n')
            print(total)


if __name__ == '__main__':
    #prase_text和prase_text2两个函数都可以实现
    # prase_text('test.txt', 'result1.txt')
    prase_text2('file_text/pipeline_search_results_20220801_125854_sg.txt', 'TrendingSearch_sg_Trending.txt')
