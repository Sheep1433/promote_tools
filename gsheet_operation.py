# 实现google API调用
import pygsheets

client = pygsheets.authorize(service_file = "testpygsheets.json")
# 打开谷歌表格testPygSheets
sh = client.open('testPygSheets')
#获取表格中的而第一张工作表
wks = sh.sheet1
# 更新A1数据
wks.update_value('A1', "我是元素A1")

# wks.update_values_batch(['A1:A2', 'B1:B2'], [[[1],[2]], [[3],[4]]])
# print(wks.get_values_batch(['A1:B1', 'B1:B2']))

wks.update_values('A1:A3', [[1],[3], [5]])

# res
# for row in range(10):
#     s = f"A1:B1"
for row in range(10):
    for col in range(5):
        c = chr(col + 65)
        cell = c + str(row)
        wks.update_value(cell, cell)
