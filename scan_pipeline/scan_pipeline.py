# -*- coding: utf-8 -*-
# @Description :
# @Time        : 2022/7/17
# @Author      : Shirley.Zhang
import logging
import os

import requests

import info
from utils.seatalk_utils import send_seatalk_message_by_groupbot

# 单人的机器人
qa_bot_id = "P6YxRdmqQNyjuJJSxGVU1Q"
# 搜推报告的群机器人
qa_bot_id_1 = "uUAdHD8tRZW08VVF67eyYw"

def scan_pipeline(service_id):
    headers = {
        'authority': 'space.shopee.io',
        'accept': '*/*',
        'authorization': 'Bearer ' + info.token,
    }

    params = {
        'service_id': service_id,
        'with_jenkins_job': 'true',
    }
    response = requests.get('https://space.shopee.io/apis/deploy/v1/pipeline/record/list', params=params,
                            headers=headers)
    res = response.json()
    print(res)
    return response.status_code, res


def trigger_demo_pipeline():
    cookies = {'JSESSIONID.c5c95112': 'node04ilwezlkentj1x2cyfywd5xw24.node0'}
    headers = {'Authorization': 'Basic YnVpbGRfdXNlcjoxMTY5MzNkNzdlNWNkZmNmMTQ0OTcxZTNjMWM3NjRmNDJm'}
    params = {'token': '116933d77e5cdfcf144971e3c1c764f42f'}
    response = requests.get('http://10.143.252.172:8080/job/hello_world/build', params=params, cookies=cookies,
                            headers=headers)
    print(response.status_code)


# 触发跑test环境的接口自动化ci
def trigger_search_rcmd_test_ci():
    # headers = {'Authorization': 'Basic YnVpbGRfdXNlcjoxMTY5MzNkNzdlNWNkZmNmMTQ0OTcxZTNjMWM3NjRmNDJm'}
    params = {'token': '12345'}
    response = requests.get('http://10.12.208.40:9999/generic-webhook-trigger/invoke?token=12345', params=params)
    return response.status_code


def scan_all_project_testenv_pipeline():
    notice_msg = "test环境自动化部署更新如下：" + "\n"
    trigger_flag = False
    for item in info.cmdb_info:
        pipeline_list_code, pipeline_list_res = scan_pipeline(info.cmdb_info[item]['service_id'])
        if pipeline_list_code == 200:
            print("get service pipe list success: " + str(pipeline_list_code) + " " + str(item))
            for branch in info.cmdb_info[item]['branch']:
                result_id = 0
                for i in pipeline_list_res['result']['in_active_pipeline_records']:
                    if i["env"] == 'test' and i["result"] == 'SUCCESS' and i["parameters"][
                        'FROM_BRANCH'] == branch:
                        result_id = i['id']
                        break
                if result_id != 0:
                    file_name = './records/' + str(item) + '_' + branch[branch.find("/") + 1:] + '_test.txt'
                    if not os.path.exists(file_name):
                        with open(file_name, 'w') as f:
                            f.write(str(result_id))
                    else:
                        with open(file_name, 'r') as f:
                            last_id = f.read()
                        if int(last_id) != result_id:
                            trigger_flag = True
                            notice_msg += item + ' -- ' + branch + '\n'
                            with open(file_name, 'w') as f:
                                f.write(str(result_id))
        else:
            print("get service pipe list fail:" + str(pipeline_list_code) + " " + str(item))
            logging.error("get service pipe list fail:" + str(pipeline_list_code) + " " + str(item))
            send_seatalk_message_by_groupbot(qa_bot_id, "token失效，无法获取服务部署信息")
            break
    # 如果有更新，则发送自动化部署更新的服务list，以及触发接口自动化ci
    if trigger_flag:
        print(1)
        # send_seatalk_message_by_groupbot(qa_bot_id, notice_msg)
        # send_seatalk_message_by_groupbot(qa_bot_id_1, notice_msg)
        # run_res = trigger_search_rcmd_test_ci()
        # print("trigger result for search_rcmd-test ci: " + str(run_res))


if __name__ == '__main__':
    scan_all_project_testenv_pipeline()
    # print(scan_pipeline(17867))