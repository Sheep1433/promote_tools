# -*- coding: utf-8 -*-
# @Description :
# @Time        : 2022/4/25
# @Author      : Shirley.Zhang
import logging

import requests

"""
    sample:
    curl -i -X POST -H 'Content-Type: application/json' -d '{ "tag": "text", "text": {"content": "Hello World!"}}' 
    https://openapi.seatalk.io/webhook/group/JuF3JChXSZG0dfyFpZeECg
"""
# QA_bot_url = "https://openapi.seatalk.io/webhook/group/JuF3JChXSZG0dfyFpZeECg"
headers = {'Content-Type': 'application/json'}
base_url = "https://openapi.seatalk.io/webhook/group/"


def send_seatalk_message_by_groupbot(bot_id, message):
    json_data = {
        'tag': 'text',
        'text': {
            'content': message
        },
    }
    url = base_url + bot_id
    try:
        response = requests.post(url, headers=headers,
                                 json=json_data)
        return response
    except Exception as e:
        logging.error("send seatalk message fail")
        logging.error(e)
