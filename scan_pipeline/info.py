# -*- coding: utf-8 -*-
# @Description :
# @Time        : 2022/7/19
# @Author      : Shirley.Zhang

token = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImI3OTBlYjE5MmY2YTJkMTZjNDQwZWE3NGRlODU4ODg1NDc5NjZmZDEiLCJ0eXAiOiJKV1QifQ.eyJlbWFpbCI6ImRhbi53YW5nQHNob3BlZS5jb20iLCJleHAiOjE2NjMyMjgzNDIsImlzcyI6Imh0dHBzOi8vc3BhY2Uuc2hvcGVlLmlvL3YxL2NlcnRzIiwic3ViIjoiNDkwNTAyIn0.iFm1F6RH71ZuHTLx7Eh-EpE9WfF18MNevM5iD_JfGsFq4ZvUaRnYav4zQ2L3SC8ZwhOYx8AKT1xi1B76Lg_nq83dr-glo3VAfeweTrRkdwHsoaU0O1Boett7Se1DVAwCvpY6GTGqPR2AsE0qqvl4Sp0rQRdqGyg4HkRAgf7Q005zT_MWJMA483j41jN-EKoiOmokEgi7DfahANtoRLrj-O6RlVH9AJiUtGR4EGL84gc_rakW8jCmz-VQEOzlnRVPjNuDlLNH24QvpA_EiOij6bKX2Pviq1cxIFYl7xXxb5ysuPug0y74raQSaXd6hLHAriRff00d0s2wAgdRNAQ3TkcomPvBQWTZY20Ej9vsbu29juiS2YZKHybZsUCElV182Y0uD_T1Y4uHVkHnDQ-1VkfI5oLPu3LLs7HGSnLS2ghttLbaGO7vWr5EHHYhET-HbTx6qtC8_fgNAj4nWhui61-J9QnIhKz5WOPFpchRFxqcGOHQbB-R4hxSSGW2QDPOeitAtBwCyjyCyCvBuuxq_c7D587dKRpMpcE6-QBlKfLcNRDL-VNI_UWIiThr9Po9mQU24psggRhPLfl1CmCLW_BbodCDevXVch555O80RAA6gsiD5Cncj2x6_kdJmLxPnMhUg2UjOTpEEmAIeiKPha_kVo7UNlBr2K7bLQuXbq8"
# cmdb_info = {'featuremanager': {'service_id': '17039', 'branch': ['origin/preonline', 'origin/master']},
#              'inferencemanager': {'service_id': '25265', 'branch': ['origin/preonline', 'origin/master']},
#              'modelmanager': {'service_id': '16638', 'branch': ['origin/preonline', 'origin/master']},
#              'trainingmanager': {'service_id': '17107', 'branch': ['origin/preonline', 'origin/master']}}
cmdb_info = {'o2oquersugg': {'service_id':17867, 'branch':['origin/master']},
             'recsyssearch':{'service_id':14063, 'branch':['origin/master']},
             'searchsvr': {'service_id':24877, 'branch':['origin/master']},
             'shadingword':{'service_id':20612, 'branch':['origin/master']},
             'o2oqueryprocess':{'service_id':18126, 'branch':['origin/master']},
             'o2ogateway':{'service_id':17868, 'branch':['origin/master']},
             'foodgateway': {'service_id': 16987, 'branch': ['origin/master']}
             }
