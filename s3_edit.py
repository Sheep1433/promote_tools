import os

import yaml
from boto3.s3.transfer import S3Transfer
#
with open('s3.yaml', 'r') as fi:
    config = yaml.load(fi)
s3_file = config['s3']

STAGE_AWS_ACCESS_KEY = "WU0HU9JXXBCDH6QH39PC"
STAGE_AWS_SECRET_KEY = "GZHktdlWWx18mNx0HOY7hhfkOgHhpn5Ms5QfvEDw"
STAGE_HOST = "http://s3.staging.shopeemobile.com"
STAGE_BUCKET_ID = "shopee_toc_dp_staging"

import boto3

client = boto3.resource(service_name="s3", aws_access_key_id=STAGE_AWS_ACCESS_KEY,
                            aws_secret_access_key=STAGE_AWS_SECRET_KEY, endpoint_url=STAGE_HOST)

# for bucket in client.buckets.all():
#     print(bucket.name)
#     for obj in bucket.objects.all():
#         print(obj.key)

local_file_name = "/tmp/hello.txt"
client.Object(STAGE_BUCKET_ID, "querysug_vocab_v2_test_id.csv").download_file(local_file_name)

def file2S3(aws_access_key, aws_secret_key, host, bucket_id, object_name, local_file_path):
    client = boto3.client('s3',
                          aws_access_key_id=aws_access_key,
                          aws_secret_access_key=aws_secret_key,
                          endpoint_url=host,
                          )
    s3 = S3Transfer(client)
    s3.upload_file(
        filename=local_file_path,
        bucket=bucket_id,
        key=object_name
    )


def hdfs2spark():
    destination_path = "/ldap_home/chenruixiang/query_suggestion/data/"
    base_hdsf_path = "hdfs://R2/projects/digitalpurchase_algo/hdfs/dev/chenruixiang/query_suggestion/data/"
    file = 'querysug_vocab_v2_test_sg.csv'
    exit_path = destination_path + file
    if os.path.exists(exit_path):
        os.system("rm -rf " + exit_path)
    os.system(
        "hadoop fs -get {}{} ".format(base_hdsf_path, file) + destination_path)
    os.system("ls -a")
    return

if __name__ == "__main__":
    hdfs2spark()
